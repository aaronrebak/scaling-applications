# scaling-applications

## How it works

1. The `.gitlab-ci.yaml` calls `main.py`
2. `main.py` creates some data (this could be a query or something more useful) and a Bokeh plot.
