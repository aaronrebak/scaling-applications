from bokeh.plotting import figure, show, output_file
from bokeh.models import ColumnDataSource, HoverTool, Div
from bokeh.layouts import column

# Output file
output_file("public/scaling_applications.html", "Aaron Rebak - Scaling Applications")

# Define ranges for quadrants
quadrant_data = [
    {
        "name": "The Starter", "x_range": (0, 3), "y_range": (0, 16),
        "coordinate_data": [
            {"x": 0.5, "y": 4, "cpu": "0.25 CPU", "memory": "512 MB", "task_count": 1, "requests_per_second": 100, "monthly_cost": "13.48"}
        ]
    },
    {
        "name": "The Expander", "x_range": (3, 6), "y_range": (0, 16),
        "coordinate_data": [
            {"x": 3.5, "y": 4, "cpu": "0.25 CPU", "memory": "512 MB", "task_count": 3, "requests_per_second": 300, "monthly_cost": "40.44"},
            {"x": 5.5, "y": 4, "cpu": "0.25 CPU", "memory": "512 MB", "task_count": 6, "requests_per_second": 600, "monthly_cost": "80.88"},
            {"x": 3.5, "y": 12, "cpu": "0.5 CPU", "memory": "1 GB", "task_count": 3, "requests_per_second": 900, "monthly_cost": "80.64"},
            {"x": 5.5, "y": 12, "cpu": "0.5 CPU", "memory": "1 GB", "task_count": 6, "requests_per_second": 1800, "monthly_cost": "161.28"}
        ]
    },
    {
        "name": "The Elevator", "x_range": (0, 6), "y_range": (16, 32),
        "coordinate_data": [
            {"x": 0.5, "y": 20, "cpu": "4 CPU", "memory": "8 GB", "task_count": 1, "requests_per_second": 8100, "monthly_cost": "268.03"},
            {"x": 0.5, "y": 28, "cpu": "8 CPU", "memory": "16 GB", "task_count": 1, "requests_per_second": 24300, "monthly_cost": "536.06"}
        ]
    },
    {
        "name": "The Overkiller", "x_range": (3, 6), "y_range": (16, 32),
        "coordinate_data": [
            {"x": 3.5, "y": 20, "cpu": "4 CPU", "memory": "8 GB", "task_count": 3, "requests_per_second": 24300, "monthly_cost": "804.09"},
            {"x": 5.5, "y": 20, "cpu": "4 CPU", "memory": "8 GB", "task_count": 6, "requests_per_second": 48600, "monthly_cost": "1608.18"},
            {"x": 3.5, "y": 28, "cpu": "8 CPU", "memory": "16 GB", "task_count": 3, "requests_per_second": 72900, "monthly_cost": "1608.18"},
            {"x": 5.5, "y": 28, "cpu": "8 CPU", "memory": "16 GB", "task_count": 6, "requests_per_second": 145800, "monthly_cost": "3216.36"}
        ]
    }
]

# Generate data points
x_points = []
y_points = []
quadrant_labels = []
cpu = []
memory = []
task_count = []
requests_per_second = []
monthly_cost = []

for quad in quadrant_data:
    name = quad["name"]
    coordinate_data = quad["coordinate_data"]

    for coordinate in coordinate_data:
        x_point = coordinate["x"]
        y_point = coordinate["y"]
        cpu_data = coordinate["cpu"]
        memory_data = coordinate["memory"]
        task_count_data = coordinate["task_count"]
        requests_per_second_data = coordinate["requests_per_second"]
        monthly_cost_data = coordinate["monthly_cost"]

        x_points.append(x_point)
        y_points.append(y_point)
        quadrant_labels.append(name)
        cpu.append(cpu_data)
        memory.append(memory_data)
        task_count.append(task_count_data)
        requests_per_second.append(requests_per_second_data)
        monthly_cost.append(monthly_cost_data)

# Create ColumnDataSource
source = ColumnDataSource(data={
    'x': x_points,
    'y': y_points,
    'quadrant': quadrant_labels,
    'cpu': cpu,
    'memory': memory,
    'task_count': task_count,
    'requests_per_second': requests_per_second,
    'monthly_cost': monthly_cost
})

# Create figure
p = figure(title="Scaling Applications", x_axis_label='Horizontal Scaling', y_axis_label='Vertical Scaling',
           x_range=(0, 6), y_range=(0, 32), tools="", toolbar_location=None)

# Add scatter plot (visible markers)
scatter = p.scatter(x='x', y='y', size=10, source=source, fill_alpha=0.6, line_alpha=0.6, marker='circle', color="black")

# Add hover tool
hover = HoverTool(renderers=[scatter], tooltips=[
    ("Quadrant", "@quadrant"),
    ("CPU", "@cpu"),
    ("Memory", "@memory"),
    ("Task Count", "@task_count"),
    ("Requests Per Second", "@requests_per_second"),
    ("Monthly Cost($)", "@monthly_cost")
])
p.add_tools(hover)

# Draw axes
p.line(x=[3, 3], y=[0, 32], line_dash="dashed", line_width=2, color="black")
p.line(x=[0, 6], y=[16, 16], line_dash="dashed", line_width=2, color="black")

p.xaxis.major_label_text_font_size = '0pt'  # Hide x-axis numbers
p.yaxis.major_label_text_font_size = '0pt'  # Hide y-axis numbers
p.xaxis.major_tick_line_color = None
p.yaxis.major_tick_line_color = None
p.xaxis.minor_tick_line_color = None
p.yaxis.minor_tick_line_color = None

# Overlay quadrant names
p.text(x=[1.5, 4.5, 1.5, 4.5], y=[8, 8, 24, 24],
       text=["The Starter", "The Expander", "The Elevator", "The Overkiller"],
       text_align="center", text_baseline="middle", text_font_size="10pt", text_color="gray")

# Assumptions text
assumptions_text = """
<h2>Assumptions for Requests Per Second Scaling:</h2>
<p><strong>Baseline:</strong></p>
<ul>
<li>0.25 CPU, 512 MB: 100 RPS (baseline per task).</li>
</ul>
<p><strong>Scaling:</strong></p>
<ul>
<li>CPU: RPS scales linearly with the number of CPUs.</li>
<li>Memory: RPS scales by a factor of 1.5 for each doubling of memory.</li>
</ul>
<h2>Calculations:</h2>
<ul>
<li><strong>0.25 CPU, 512 MB:</strong> Baseline: 100 RPS per task.</li>
<li><strong>0.5 CPU, 1 GB:</strong> CPU is doubled: 100 RPS * 2 = 200 RPS. Memory is doubled: 200 RPS * 1.5 = 300 RPS per task.</li>
<li><strong>4 CPU, 8 GB:</strong> CPU is 16 times: 100 RPS * 16 = 1600 RPS. Memory is 16 times: 1600 RPS * 1.5 * 1.5 * 1.5 * 1.5 = 8100 RPS per task.</li>
<li><strong>8 CPU, 16 GB:</strong> CPU is 32 times: 100 RPS * 32 = 3200 RPS. Memory is 32 times: 3200 RPS * 1.5 * 1.5 * 1.5 * 1.5 * 1.5 = 24300 RPS per task.</li>
</ul>
"""

div = Div(text=assumptions_text, width=800)

# Layout
layout = column(p, div)

# Show plot with assumptions
show(layout)
